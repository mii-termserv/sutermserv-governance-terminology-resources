{
  "resourceType": "CodeSystem",
  "id": "mii-cs-suts-resource-tags-license",
  "meta": {
    "profile": [
      "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
    ],
    "tag": [
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project",
        "code": "sutermserv",
        "display": "Service Unit Terminological Services"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset",
        "code": "sutermserv",
        "display": "Service Unit Terminological Services"
      },
      {
        "system": "https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
        "code": "https://mii-termserv.de/licenses/cc0-1.0",
        "display": "CC0 1.0 Universal"
      }
    ]
  },
  "extension": [
    {
      "url": "http://hl7.org/fhir/StructureDefinition/cqf-scope",
      "valueString": "@mii-termserv/de.sutermserv.governance.terminology"
    }
  ],
  "url": "http://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license",
  "version": "20250120",
  "name": "MII_CS_SUTS_ResourceTags_License",
  "title": "MII CS SUTS Resource-Tags Licenses",
  "status": "active",
  "experimental": false,
  "date": "2025-01-20",
  "publisher": "MII SU-TermServ",
  "contact": [
    {
      "name": "SU-TermServ Team",
      "telecom": [
        {
          "system": "email",
          "value": "team@mail.mii-termserv.de"
        }
      ]
    }
  ],
  "description": "A CodeSystem to identify license terms of especially CodeSystem resources uploaded to the MII SU-TermServ terminology server. The codes are resolveable URLs that state the license terms of the resource on our website.",
  "copyright": "CC0-1.0",
  "valueSet": "http://mii-termserv.de/fhir/su-termserv/ValueSet/mii-cs-suts-resource-tags-license",
  "hierarchyMeaning": "part-of",
  "compositional": false,
  "content": "complete",
  "count": 34,
  "property": [
    {
      "code": "added-in-version",
      "description": "The version of the CS this concept was added",
      "type": "string"
    }
  ],
  "concept": [
    {
      "code": "https://mii-termserv.de/licenses#atc-who",
      "display": "ATC/WHO license terms",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240710"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#bfarm",
      "display": "BfArM license terms",
      "definition": "https://www.bfarm.de/SharedDocs/Downloads/DE/Kodiersysteme/downloadbedingungen-2023.pdf?__blob=publicationFile",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240513"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#bfarm-icd-o-3",
      "display": "BfArM license terms for ICD-O-3",
      "definition": "https://www.bfarm.de/SharedDocs/Downloads/DE/Kodiersysteme/downloadbedingungen-icd-o-3.pdf?__blob=publicationFile",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240513"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#bfarm-icf",
      "display": "BfArM license terms for ICF",
      "definition": "https://www.bfarm.de/SharedDocs/Downloads/DE/Kodiersysteme/downloadbedingungen-icf.pdf?__blob=publicationFile",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240513"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cas",
      "display": "CAS registry license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240730"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc0-1.0",
      "display": "CC0-1.0 Universal",
      "definition": "https://creativecommons.org/publicdomain/zero/1.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-4.0",
      "display": "CC-BY 4.0",
      "definition": "https://creativecommons.org/licenses/by/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-nc-4.0",
      "display": "CC-BY-NC 4.0",
      "definition": "https://creativecommons.org/licenses/by-nc/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-nc-nd-4.0",
      "display": "CC-BY-NC-ND 4.0",
      "definition": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-nc-sa-4.0",
      "display": "CC-BY-NC-SA 4.0",
      "definition": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-nd-4.0",
      "display": "CC-BY-ND 4.0",
      "definition": "https://creativecommons.org/licenses/by-nd/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#cc-by-sa-4.0",
      "display": "CC-BY-SA 4.0",
      "definition": "https://creativecommons.org/licenses/by-sa/4.0/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#dicom",
      "display": "DICOM license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240619"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#edqm",
      "display": "EDQM license terms",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240709"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#fdpg-internal",
      "display": "FDPG+ Internal",
      "definition": "The resources with this license are intended to only be used in the FDPG+ project and platform, but no use restrictions apply.",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20241113"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#fhir",
      "display": "HL7 FHIR License",
      "definition": "http://www.hl7.org/fhir/license.html",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#gecco",
      "display": "GECCO license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240513"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#hl7-de",
      "display": "HL7 Germany license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240731"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#ietf-iana",
      "display": "IETF/IANA license terms",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240702"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#ihe",
      "display": "Integrating the Healthcare Enterprise (IHE) license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240515"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#iso",
      "display": "ISO standards licenses",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240702"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#kbv",
      "display": "KBV license terms",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240710"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#kbv-schluesseltabellen",
      "display": "KBV license for Schlüsseltabellen",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240514"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#loinc",
      "display": "LOINC license",
      "definition": "https://loinc.org/kb/license/",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#mii-cds",
      "display": "MII CDS license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#oncotree",
      "display": "OncoTree",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20241205"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#orphanet",
      "display": "Orphanet/Orphadata license terms",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240715"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#pzn",
      "display": "PZN license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240702"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#rtmms",
      "display": "RTMMS License for ISO/IEEE 11073",
      "definition": "https://rtmms.nist.gov/gb_users/register",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240514"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#snomed-ct",
      "display": "SNOMED CT license",
      "definition": "https://www.bfarm.de/EN/Code-systems/Terminologies/SNOMED-CT/affiliate-licence/_node.html",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#tho",
      "display": "THO license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240510"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#ucum",
      "display": "UCUM license",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240701"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#unavailable",
      "display": "Unavailable",
      "definition": "This concept is used to indicate that the resources aren't available in FHIR format",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240710"
        }
      ]
    },
    {
      "code": "https://mii-termserv.de/licenses#unclear-open",
      "display": "Open unclear license",
      "definition": "This concept is used to indicate that the resources are available under an open license, but the exact license ters are unclear. It can be reasonably presumed that any use of this artefact is unproblematic.",
      "property": [
        {
          "code": "added-in-version",
          "valueString": "20240820"
        }
      ]
    }
  ],
  "text": {
    "status": "generated",
    "div": "<div xmlns=\"http://www.w3.org/1999/xhtml\"><h2>MII CS SUTS Resource-Tags Licenses</h2><tt>http://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license</tt><p>A CodeSystem to identify license terms of especially CodeSystem resources uploaded to the MII SU-TermServ terminology server. The codes are resolveable URLs that state the license terms of the resource on our website.</p><div><p><b>SU-TermServ Metadata</b></p><table><tbody><tr><td><b>Project</b></td><td>Service Unit Terminological Services</td></tr><tr><td><b>Dataset</b></td><td>Service Unit Terminological Services</td></tr><tr><td><b>License</b></td><td><a href=\"https://mii-termserv.de/licenses/cc0-1.0\">CC0 1.0 Universal</a></td></tr><tr><td><b>Package Scope</b></td><td><a href=\"https://gitlab.com/groups/mii-termserv/fhir-resources/-/packages/?ForderBy%3Dversion%26sort%3Ddesc%26search%5B%5D%3D@mii-termserv/de.sutermserv.governance.terminology\"><code>@mii-termserv/de.sutermserv.governance.terminology</code></a></td></tr></tbody></table></div></div>"
  }
}