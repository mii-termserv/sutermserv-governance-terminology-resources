Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: version
* ^version = "20250120"
* ^date = "2025-01-20"

RuleSet: common(id_fragment)
* ^meta.profile[+] = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = "sutermserv"
* ^meta.tag[=].display = "Service Unit Terminological Services"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = "sutermserv"
* ^meta.tag[=].display = "Service Unit Terminological Services"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = "https://mii-termserv.de/licenses/cc0-1.0"
* ^meta.tag[=].display = "CC0 1.0 Universal"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/de.sutermserv.governance.terminology"
* ^url = "http://mii-termserv.de/fhir/su-termserv/CodeSystem/{id_fragment}"
* ^valueSet = "http://mii-termserv.de/fhir/su-termserv/ValueSet/{id_fragment}"
* insert version
* ^content = #complete
* ^experimental = false
* ^status = #active
* ^contact[+].name = "SU-TermServ Team"
* ^contact[=].telecom[+].system = "email"
* ^contact[=].telecom[=].value = "team@mail.mii-termserv.de"
* ^copyright = "CC0-1.0"
* ^hierarchyMeaning = #part-of
* ^publisher = "MII SU-TermServ"
* ^compositional = false

RuleSet: common-props-added-in-version
* ^property[+].code = #added-in-version
* ^property[=].type = #string
* ^property[=].description = "The version of the CS this concept was added"

RuleSet: common-props-notSelectable
* ^property[+].code = #notSelectable
* ^property[=].type = #boolean
* ^property[=].description = "Indicates that this concept is abstract/not selectable"
* ^property[=].uri = "http://hl7.org/fhir/concept-properties#notSelectable"

CodeSystem: MII_CS_SUTS_ResourceTags_Dataset
Title: "MII CS SUTS Resource-Tags Datasets"
Id: mii-cs-suts-resource-tags-dataset
Description: "A list of known data sets for tagging resources uploaded to the MII SU-TermServ terminology server. The additional properties in this resource may be used for creating template FHIR resources that conform to the standards of the SU-TermServ."
* insert common("mii-cs-suts-resource-tags-dataset")
* insert common-props-added-in-version
* insert common-props-notSelectable
* ^property[+].code = #technical-module-name
* ^property[=].type = #string
* ^property[=].description = "The 'technical module name' for generation the canonical URL and ID"
* ^property[+].code = #abbreviation
* ^property[=].type = #string
* ^property[=].description = "An abbreviation in the usual capitalization used for generation of title, name and ID"
* ^property[+].code = #namespace
* ^property[=].type = #string
* ^property[=].description = "The namespace of this dataset, used in the generation of all metadata elements"
/*^babelfsh
csv --path='./datasets.csv'
    --code-column="code"
    --display-column="display"
    --definition-column="definition"
    --property-mapping=[{"property":"parent","column":"property_parent"},{"property":"technical-module-name","column":"property_technical-module-name"},{"property":"abbreviation","column":"property_abbreviation"},{"property":"namespace","column":"property_namespace"},{"property":"added-in-version","column":"property_added-in-version","required":true},{"property":"notSelectable","column":"property_notSelectable"}]
^babelfsh*/

CodeSystem: MII_CS_SUTS_ResourceTags_Project
Title: "MII CS SUTS Resource-Tags Projects"
Id: mii-cs-suts-resource-tags-project
Description: "A CodeSystem for the use in resource tags to identify the project a resource is maintained in."
* insert common("mii-cs-suts-resource-tags-project")
* insert common-props-added-in-version
* insert common-props-notSelectable
* ^property[+].code = #url-prefix
* ^property[=].type = #string
* ^property[=].description = "An URL prefix that can be used for generating canonical URLs for resources in this project"
/*^babelfsh
csv --path='./projects.csv'
    --code-column="code"
    --display-column="display"
    --definition-column="definition"
    --property-mapping=[{"property":"parent","column":"property_parent"},{"property":"url-prefix","column":"property_url-prefix"},{"property":"added-in-version","column":"property_added-in-version","required":true},{"property":"notSelectable","column":"property_notSelectable"}]
^babelfsh*/

CodeSystem: MII_CS_SUTS_ResourceTags_License
Title: "MII CS SUTS Resource-Tags Licenses"
Id: mii-cs-suts-resource-tags-license
Description: "A CodeSystem to identify license terms of especially CodeSystem resources uploaded to the MII SU-TermServ terminology server. The codes are resolveable URLs that state the license terms of the resource on our website."
* insert common("mii-cs-suts-resource-tags-license")
* insert common-props-added-in-version
/*^babelfsh
csv --path='./licenses.csv'
    --code-column="code"
    --display-column="display"
    --definition-column="definition"
    --property-mapping=[{"property":"added-in-version","column":"property_added-in-version","required":true}]
^babelfsh*/
