# SU-TermServ Governance Terminology Resources

This repository contains the terminology resources for the SU-TermServ project's governance.

Using GitLab CI, this repository is automatically published as a NPM package to the GitLab registry.

This package can be installed by creating a file `.npmrc` in the root of your project with the following content:

```
@mii-termserv:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/
//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}
```

and installing the package using `npm install @mii-termserv/de.sutermserv.governance.terminology`.

## Change Log

### 2.0.2

Add Oncotree and MSKCC concepts

### 2.0.1

Add FDPG+ internal concept

### 2.0.0

Change generation to [BabelFSH](https://gitlab.com/mii-termserv/babelfsh); add CS resource for licenses; add several concepts

### 1.3.0 (2024-05-07)

Add parent codes to both CS.

### 1.2.1 (2024-05-07)

remove undefined parent code 'num' in dataset CS

### 1.2.0 (2024-05-06)

Change to correct package structure

### 1.1.7 (2024-04-17)

Add WHO concepts

### 1.1.6 (2024-04-16)

Add international IGs to the CodeSystem resources.

### 1.1.5 (2024-04-04)

Hotfix: Swap BBMRI concepts between Dataset and Project.

### 1.1.4 (2024-04-04)

Added BBMRI-GBA concepts.

### 1.1.3 (2024-03-19)

Added an IHE concept to Datasets; remove dependency as it is unused.

### 1.1.2 (2024-03-18)

Change NPM identifier (was: `sutermserv-governance-terminology-resources`) to: `de.sutermserv.governance.terminology` (within namespace `@mii-termserv`).

### 1.1.1 (2024-03-15)

Change dependency for R4B to su-termserv fork of R4B terminology.

### 1.1.0 (2024-03-07)

Add concepts to the CodeSystem resources

### 1.0.0 (2023-11-03)

initial release with two CodeSystem resources

