#!/bin/zsh

function babelfsh() {
  jar_file="$(realpath babelfsh.jar)"
  java -jar $jar_file $@
}

babelfsh convert -i $(realpath .) -o $(realpath package)
